package com.alexis.falabellafinanciero.Adapter.Lista

import android.content.Context
import android.content.DialogInterface
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.alexis.falabellafinanciero.Model.Lista.CurrencyDynamic
import com.alexis.falabellafinanciero.R
import com.alexis.falabellafinanciero.currency_list_item
import kotlinx.android.synthetic.main.currency_list_item.view.*


class ListaAdapter(var c: Context, var lists: ArrayList<CurrencyDynamic>,  private val clickListener: (CurrencyDynamic) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var v = LayoutInflater.from(c).inflate(R.layout.currency_list_item, parent, false)

        return Item(v)
    }

    override fun getItemCount(): Int {
        return lists.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as Item).bindData(lists[position],clickListener)
    }

    class Item(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bindData(_list: CurrencyDynamic, clickListener: (CurrencyDynamic) -> Unit) {
            itemView.tv_nombre.text = _list.nombre
            itemView.tv_valor.text = _list.valor

            itemView.setOnClickListener { clickListener(_list)}
        }
    }
}