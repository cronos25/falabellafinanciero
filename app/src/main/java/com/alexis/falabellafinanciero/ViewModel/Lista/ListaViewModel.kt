package com.alexis.falabellafinanciero.ViewModel.Lista

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.alexis.falabellafinanciero.Repository.ListaRepository
import com.alexis.falabellafinanciero.Retrofit.Response.ListResponse

class ListaViewModel : ViewModel() {

    var listRepository: ListaRepository = ListaRepository()
    private var listResponseLiveData: LiveData<ListResponse> = listRepository.getListCurrency()!!

    fun getListaResponseLiveData(): LiveData<ListResponse> {
        return listResponseLiveData
    }
}