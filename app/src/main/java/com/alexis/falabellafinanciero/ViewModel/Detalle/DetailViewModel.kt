package com.alexis.falabellafinanciero.ViewModel.Detalle

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.alexis.falabellafinanciero.Constants.AppConstants.Companion.DETAIL_CODE
import com.alexis.falabellafinanciero.Repository.DetailRepository
import com.alexis.falabellafinanciero.Retrofit.Response.DetailResponse

class DetailViewModel  : ViewModel() {
    var codigo:String = ""
    var detailRepository: DetailRepository = DetailRepository()
    private var detailResponseLiveData: LiveData<DetailResponse> = detailRepository.getDetailCurrency()

    fun getDetailResponseLiveData(): LiveData<DetailResponse> {
        return detailResponseLiveData
    }
}