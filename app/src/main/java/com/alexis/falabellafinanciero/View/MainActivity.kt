package com.alexis.falabellafinanciero.View

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alexis.falabellafinanciero.Adapter.Lista.ListaAdapter
import com.alexis.falabellafinanciero.Constants.AppConstants.Companion.DETAIL_CODE
import com.alexis.falabellafinanciero.Model.Detalle.CurrencyDetail
import com.alexis.falabellafinanciero.Model.Lista.CurrencyDynamic
import com.alexis.falabellafinanciero.R
import com.alexis.falabellafinanciero.Retrofit.Response.ListResponse
import com.alexis.falabellafinanciero.ViewModel.Lista.ListaViewModel
import java.util.*

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.simpleName
    private var my_recycler_view: RecyclerView? = null
    private var progress_circular_lista: ProgressBar? = null
    private var layoutManagerData: LinearLayoutManager? = null
    private var adapter: ListaAdapter? = null
    private val currencyArrayList: ArrayList<CurrencyDynamic> = ArrayList<CurrencyDynamic>()
    var listaViewModel: ListaViewModel? = ListaViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialization()
        getCurrencyList()
    }

    /**
     * initialization of views and others
     *
     * @param @null
     */
    private fun initialization() {
        progress_circular_lista =
            findViewById(R.id.progressBar)
        my_recycler_view = findViewById<View>(R.id.listarRecyclerView) as RecyclerView

        // use a linear layout manager
        layoutManagerData = LinearLayoutManager(this@MainActivity)

        my_recycler_view!!.layoutManager = layoutManagerData

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        my_recycler_view!!.hasFixedSize()

        // adapter
        adapter = ListaAdapter(this@MainActivity, currencyArrayList, {currency -> currencyItemClicked(currency)})
        my_recycler_view!!.setAdapter(adapter)

        // View Model
        listaViewModel =
            ViewModelProviders.of(this).get(ListaViewModel::class.java)

    }

    /**
     * get movies articles from news api
     *
     * @param @null
     */
    private fun getCurrencyList() {
        listaViewModel!!.getListaResponseLiveData().observe(this,Observer<ListResponse>{ listaResponse ->
            if ( listaResponse != null){

                progress_circular_lista?.visibility = View.GONE
                val currencies: List<CurrencyDynamic> = listaResponse.currency!!

                currencyArrayList.addAll(currencies)
                adapter!!.notifyDataSetChanged()
            }
        })
    }

    private fun currencyItemClicked(currency: CurrencyDynamic) {
        Toast.makeText(this, "Clicked: ${currency.nombre}", Toast.LENGTH_SHORT).show()
        DETAIL_CODE = currency.codigo.replace("\"","")
        val intent = Intent(this, DetailActivity::class.java)
        startActivity(intent)

    }
}