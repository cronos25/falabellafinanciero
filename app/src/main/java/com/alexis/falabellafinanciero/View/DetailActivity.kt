package com.alexis.falabellafinanciero.View

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.alexis.falabellafinanciero.Adapter.Lista.ListaAdapter
import com.alexis.falabellafinanciero.Model.Detalle.CurrencyDetail
import com.alexis.falabellafinanciero.Model.Lista.CurrencyDynamic
import com.alexis.falabellafinanciero.R
import com.alexis.falabellafinanciero.Retrofit.Response.DetailResponse
import com.alexis.falabellafinanciero.Retrofit.Response.ListResponse
import com.alexis.falabellafinanciero.ViewModel.Detalle.DetailViewModel
import com.alexis.falabellafinanciero.ViewModel.Lista.ListaViewModel
import java.util.ArrayList

class DetailActivity : AppCompatActivity() {

    private val TAG = DetailActivity::class.java.simpleName
    private var codigotv: TextView? = null
    private var uMedTv: TextView? = null
    private var nombreTv: TextView? = null
    private var progress_circular_detail: ProgressBar? = null
    private val currencyDetailItem: CurrencyDetail = CurrencyDetail()
    var detalleViewModel: DetailViewModel? = DetailViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        initialization()
        getDetailItem()
    }

    private fun initialization() {
        progress_circular_detail =
            findViewById(R.id.progress_circular_detail)

        progress_circular_detail?.visibility = View.VISIBLE


        codigotv = findViewById<View>(R.id.codigotv) as TextView
        uMedTv = findViewById<View>(R.id.uMedTv) as TextView
        nombreTv = findViewById<View>(R.id.nombreTv) as TextView

        // View Model
        detalleViewModel =
            ViewModelProviders.of(this).get(DetailViewModel::class.java)
        detalleViewModel!!.codigo = currencyDetailItem.codigo
    }
    /**
     * get movies articles from news api
     *
     * @param @null
     */
    private fun getDetailItem() {

        detalleViewModel!!.getDetailResponseLiveData().observe(this,
            Observer<DetailResponse>{ detailResponse ->
            if ( detailResponse != null){

                progress_circular_detail?.visibility = View.GONE


                codigotv?.text = detailResponse.codigo
                uMedTv?.text = detailResponse.unidad_medida
                nombreTv?.text = detailResponse.nombre

            }else {

                codigotv?.text = "error al obtener datos"
                uMedTv?.text =  "error al obtener datos"
                nombreTv?.text =  "error al obtener datos"
                progress_circular_detail?.visibility = View.GONE
            }
        })
    }
}