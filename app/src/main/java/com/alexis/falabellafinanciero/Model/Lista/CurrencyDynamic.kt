package com.alexis.falabellafinanciero.Model.Lista

data class CurrencyDynamic (
    val codigo:String = "",
    val nombre:String = "",
    val fecha:String = "",
    val unidad_medida:String = "",
    val valor:String = "")