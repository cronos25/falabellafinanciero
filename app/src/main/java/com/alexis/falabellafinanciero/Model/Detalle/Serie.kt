package com.alexis.falabellafinanciero.Model.Detalle

data class Serie(
        val fecha:String = "",
        val valor:Double = 0.0
)