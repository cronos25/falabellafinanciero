package com.alexis.falabellafinanciero.Model.Detalle

data class CurrencyDetail  (
    val version:String = "",
    val autor:String = "",
    val codigo:String = "",
    val nombre:String = "",
    val fecha:String = "",
    val unidad_medida:String = "",
    val serie:List<Serie> = listOf(Serie()))