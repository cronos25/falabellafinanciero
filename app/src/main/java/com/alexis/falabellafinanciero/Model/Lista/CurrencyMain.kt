package com.alexis.falabellafinanciero.Model.Lista

data class CurrencyMain(
            val version:String = "",
            val autor:String = "",
            val fecha:String = "",
            val currency:List<CurrencyDynamic> = listOf(CurrencyDynamic()))

fun fillCurrency(actualCurrency:Any):Boolean{
    return (actualCurrency is CurrencyDynamic)
}