package com.alexis.falabellafinanciero.Retrofit.Api

import com.alexis.falabellafinanciero.Constants.AppConstants.Companion.DETAIL_CODE
import com.alexis.falabellafinanciero.Retrofit.Response.DetailResponse
import com.alexis.falabellafinanciero.Retrofit.Response.ListResponse
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.QueryMap
import retrofit2.http.QueryName

public interface Request{

    @GET("api/")
    fun getCurrencyList(): Call<ResponseBody?>?

    @GET("/")
    fun getCurrencyDetail(): Call<DetailResponse?>?

}