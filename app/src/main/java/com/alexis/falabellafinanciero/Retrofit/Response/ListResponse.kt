package com.alexis.falabellafinanciero.Retrofit.Response

import com.alexis.falabellafinanciero.Model.Lista.CurrencyDynamic
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class ListResponse{

    @SerializedName("version") @Expose var version: String? = null

    @SerializedName("autor") @Expose var autor: String? = null

    @SerializedName("fecha") @Expose var fecha: String? = null

    @SerializedName("currency") @Expose var currency: MutableList<CurrencyDynamic>? = null

}
