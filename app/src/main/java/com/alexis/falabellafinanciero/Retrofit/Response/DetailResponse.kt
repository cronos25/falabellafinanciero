package com.alexis.falabellafinanciero.Retrofit.Response

import com.alexis.falabellafinanciero.Model.Detalle.Serie
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class DetailResponse {

    @SerializedName("version") @Expose
    val version: String? = null

    @SerializedName("autor") @Expose
    val autor: String? = null

    @SerializedName("codigo") @Expose
    val codigo: String? = null

    @SerializedName("nombre") @Expose
    val nombre: String? = null

    @SerializedName("unidad_medida") @Expose
    val unidad_medida: String? = null

    @SerializedName("serie") @Expose
    val serie: List<Serie?>? = null
}