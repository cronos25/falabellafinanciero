package com.alexis.falabellafinanciero.Retrofit.Api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

import com.alexis.falabellafinanciero.Constants.AppConstants.Companion.BASE_URL

class RetrofitRequest {

    companion object {

        private var retrofit: Retrofit? = null
        fun getRetrofitInstance(): Retrofit? {
            if (retrofit == null) {
                retrofit = Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
            }
            return retrofit
        }
    }
}