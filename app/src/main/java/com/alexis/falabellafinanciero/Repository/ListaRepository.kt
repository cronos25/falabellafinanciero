package com.alexis.falabellafinanciero.Repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.alexis.falabellafinanciero.Constants.AppConstants
import com.alexis.falabellafinanciero.Model.Lista.CurrencyDynamic
import com.alexis.falabellafinanciero.Retrofit.Api.Request
import com.alexis.falabellafinanciero.Retrofit.Api.RetrofitRequest
import com.alexis.falabellafinanciero.Retrofit.Response.ListResponse
import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class ListaRepository {
    private val TAG: String = com.alexis.falabellafinanciero.Repository.ListaRepository::class.java.getSimpleName()
    private var apiRequest: Request? = null
    private var builder: Retrofit.Builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(AppConstants.BASE_URL+"api/")
    init {
        apiRequest = RetrofitRequest.getRetrofitInstance()?.create(Request::class.java)
    }

    fun getListCurrency(): LiveData<ListResponse> {
        val data: MutableLiveData<ListResponse> = MutableLiveData<ListResponse>()
        val gson = GsonBuilder()
            .setLenient()
            .create()
        builder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(AppConstants.BASE_URL)
        apiRequest?.getCurrencyList()?.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(
                call: Call<ResponseBody?>,
                response: Response<ResponseBody?>
            ) {
                Log.d(
                    TAG,
                    "onResponse response:: $response"
                )
                if (response.body() != null) {

                    var currency: MutableList<CurrencyDynamic> = mutableListOf<CurrencyDynamic>()

                    val jsonObject = JsonParser().parse(response.body()!!.string()).asJsonObject
                    for (jsonValue in jsonObject.keySet()){
                        if ( jsonValue != "version" &&
                            jsonValue != "autor" &&
                            jsonValue != "fecha") {
                            val listResponseJson = JsonParser().parse(jsonObject.get(jsonValue).toString()).asJsonObject

                            val currencyItem = CurrencyDynamic(
                                listResponseJson.get("codigo").toString(),
                                listResponseJson.get("nombre").toString(),
                                listResponseJson.get("fecha").toString(),
                                listResponseJson.get("unidad_medida").toString(),
                                listResponseJson.get("valor").toString())
                            currency.add(currencyItem)

                        }
                    }

                    var datalresponse = ListResponse()
                    datalresponse.version = jsonObject.get("version").asString
                    datalresponse.autor = jsonObject.get("autor").asString
                    datalresponse.fecha = jsonObject.get("fecha").asString
                    datalresponse.currency = currency
                    data.setValue(datalresponse)

                    Log.d(
                        TAG,
                        "articles total result:: " + response.body().toString()
                    )
                    Log.d(
                        TAG,
                        "articles size:: " + response.body().toString()
                    )
                    Log.d(
                        TAG,
                        "articles title pos 0:: " + response.body()!!
                    )
                }
            }

            override fun onFailure(
                call: Call<ResponseBody?>,
                t: Throwable
            ) {
                Log.e(
                    TAG,
                    "error en llamado " + t.message
                )
                data.setValue(null)
            }
        })
        return data
    }
}