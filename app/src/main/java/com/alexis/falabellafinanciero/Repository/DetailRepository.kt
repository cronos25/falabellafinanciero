package com.alexis.falabellafinanciero.Repository

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.alexis.falabellafinanciero.Constants.AppConstants.Companion.BASE_URL
import com.alexis.falabellafinanciero.Constants.AppConstants.Companion.DETAIL_CODE
import com.alexis.falabellafinanciero.Retrofit.Api.Request
import com.alexis.falabellafinanciero.Retrofit.Api.RetrofitRequest
import com.alexis.falabellafinanciero.Retrofit.Response.DetailResponse
import com.google.gson.GsonBuilder
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class DetailRepository {
    private val TAG: String = com.alexis.falabellafinanciero.Repository.DetailRepository::class.java.getSimpleName()
    private var apiRequest: Request? = null
    private var builder: Retrofit.Builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .baseUrl(BASE_URL)

    init {
        apiRequest = RetrofitRequest.getRetrofitInstance()?.create(Request::class.java)
    }

    fun getDetailCurrency(): LiveData<DetailResponse> {
        val data: MutableLiveData<DetailResponse> = MutableLiveData<DetailResponse>()
        val gson = GsonBuilder()
            .setLenient()
            .create()

        builder = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create(gson))
            .baseUrl(BASE_URL+"api/$DETAIL_CODE/")

        apiRequest?.getCurrencyDetail()?.enqueue(object : Callback<DetailResponse?> {
            override fun onResponse(
                call: Call<DetailResponse?>,
                response: Response<DetailResponse?>
            ) {
                Log.d(
                    TAG,
                    "onResponse response:: $response"
                )
                if (response.body() != null) {

                    data.setValue(response.body())

                    Log.d(
                        TAG,
                        "articles total result:: " + response.body().toString()
                    )
                    Log.d(
                        TAG,
                        "articles size:: " + response.body().toString()
                    )
                    Log.d(
                        TAG,
                        "articles title pos 0:: " + response.body()!!
                    )
                }
            }

            override fun onFailure(
                call: Call<DetailResponse?>,
                t: Throwable
            ) {
                Log.e(
                    TAG,
                    "error en llamado " + t.message
                )
                data.setValue(null)
            }
        })
        return data
    }
}